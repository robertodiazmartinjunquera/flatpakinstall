# FLATPAKINSTALL

Esto es un script sencillo para instalar el servicio flatpak en las siguientes distribuciones de GNU/Linux:
<p></p>

<ul>
  <li>Debian</li>
  <li>Ubuntu</li>
  <li>Fedora</li>
  <li>OpenSUSE</li>
  <li>Arco Linux</li>
  <li>Kubuntu</li>
  <li>Void Linux</li>
  <li>Deepin</li>
  <li>Arch Linux</li>
  <li>Manjaro</li>
</ul>


# FUNCIONAMIENTO/INSTALACIÓN:

El script se ejecuta con permisos de ejecución en una terminal y el resto sera indicar la distribución que tienes, abajo te indico las ordenes necesarias que tienes que ejecutar.


```bash
chmod +x flatpakinstall.sh    #para dar permisos de ejecución al script
```

```bash
sudo ./flatpakinstall.sh     #Para ejecutar el script
```
