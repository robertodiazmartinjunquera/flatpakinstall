#!/bin/bash
echo
 echo "************************************************************************"
 echo "**                                                                    **"
 echo "**                           FLATPAKINSTALL                           **"  
 echo "**                                                                    **"
 echo "************************************************************************"
echo
echo "Este es el script de Roberto Díaz, para instalar flatpak en tu distribución de GNU/Linux"
echo
echo Empezemos:
sleep 3s
echo

echo  "Comprobaré que distribución usas para poder instalar flatpak:"

#VERSION="grep '^(VERSION)=' /etc/os-release"
#NAME="grep '^(NAME)=' /etc/os-release"
#ID=grep '^(ID)=' /etc/os-release
NAME=$(egrep '^(NAME)=' /etc/os-release)
#echo El nombre es: $NAME
VERSION=$(egrep '^(VERSION)=' /etc/os-release)
#echo La versión es: $VERSION
ID=$(egrep '^(ID)=' /etc/os-release)

if [ "$ID" == "ID=debian" ]
then 
echo "Tu distribución es Debian"
echo
sleep 1s
case $XDG_CURRENT_DESKTOP in
    GNOME)
        sudo apt install flatpak -y ; sudo apt install -y gnome-software-plugin-flatpak ; sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
    CINNAMONT)
        sudo apt install flatpak -y ; sudo apt install -y gnome-software-plugin-flatpak ; sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
    KDE)
        sudo apt install -y flatpak ; sudo apt install -y plasma-discover-backend-flatpak ; sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
    *)
        sudo apt install -y flatpak ; sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
esac
fi

if [ "$ID" == "ID=ubuntu" ]
then 
echo "Tu distribución es Ubuntu"
echo
sleep 1s
        sudo apt install -y flatpak
        sudo apt install -y gnome-software-plugin-flatpak
        sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

fi

if [ "$ID" == "ID=fedora" ]
then 
echo "Tu distribución es Fedora"
echo
sleep 1s
        sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
fi

if [ "$ID" == "ID=opensuse" ]
then 
echo "Tu distribución es openSUSE"
echo
sleep 1s

case $XDG_CURRENT_DESKTOP in
    GNOME)
        sudo zypper install flatpak ; sudo zypper install gnome-software-plugin-flatpak ; sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo        
        ;;
    *)
        sudo zypper install flatpak ; sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        ;;
esac
fi

if [ "$ID" == "ID=arcolinux" ]
then 
echo "Tu distribución es Arco Linux"
echo
sleep 1s
        sudo pacman -S flatpak
fi

if [ "$ID" == "ID=kubuntu" ]
then 
echo "Tu distribución es Kubuntu"
echo
sleep 1s
        sudo apt install -y flatpak
        sudo apt install plasma-discover-flatpak-backend
        sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
fi

if [ "$ID" == "ID=void-linux" ]
then 
echo "Tu distribución es Void Linux"
echo
sleep 1s
        sudo xbps-install -S flatpak
        sudo xbps-install -S xdg-desktop-portal xdg-desktop-portal-gtk xdg-user-dirs xdg-user-dirs-gtk xdg-utils
        sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
fi

if [ "$ID" == "ID=deepin" ]
then 
echo "Tu distribución es Deepin"
echo
sleep 1s
         sudo apt install flatpak
        sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
        sudo flatpak install flathub org.gtk.Gtk3theme.deepin
        sudo flatpak install flathub org.gtk.Gtk3theme.deepin
fi

if [ "$ID" == "ID=Arch Linux" ]
then 
echo "Tu distribución es Arch Linux"
echo
sleep 1s
        sudo pacman -Sy flatpak --noconfirm
fi

if [ "$ID" == "ID=manjaro" ]
then 
echo "Tu distribución es Manjaro"
echo
sleep 1s
        sudo pacman -Sy flatpak --noconfirm
fi

echo
sleep 1s
echo "Este script es realidad gracias a mi trabajo personal y cumple las cuatro libertades del software libre, que son las siguientes:"
echo ""
echo "La libertad de ejecutar el software como te plazca y con cualquier objetivo.\nLa libertad de estudiar como funciona el programa y cambiarlo a tu gusto.\nLa libertad de poder redistribuir copias del programa a los demás.\nLa libertad de poder distribuir también tus mejoras al programa original."
echo

echo "Gracias por usar mi script"
echo

echo Pues ya lo tienes, ahora porfavor deberías reiniciar para que los cambios surjan efecto. 
echo

    read -p "Quieres reiniciar el equipo?" sn
    case $sn in
       [Ss]* )  sudo reboot;;
        [Nn]* ) exit;;
        * ) echo "Por favor, pulsa s o n.";;
    esac
done
